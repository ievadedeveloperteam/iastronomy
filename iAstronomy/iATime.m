//
//  iATime.m
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <math.h>
#import "iATime.h"
#import "iAUnit.h"
#import "FirstViewController.h"

@implementation iATime


static NSMutableArray *allUnits=nil;

+(NSArray *)allUnits{
    if(allUnits==nil){
        allUnits=[[NSMutableArray alloc] init];
        iAUnit *localDate=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Local Date", @"Local title") Value:@"EEEE', 'MMM d', 'YYYY GG" IsActivated:YES];
        [allUnits addObject:localDate];
        iAUnit *localTime=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Local Time", @"Local Time title") Value:@"HH:mm:ss zzz" IsActivated:YES];
        [allUnits addObject:localTime];
        iAUnit *coordinatedUniversalTime=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Coordinated Universal Time", @"Coordinated Universal Time title") Value:@"YYYY-MM-dd' at 'HH:mm:ss" IsActivated:YES];
        [allUnits addObject:coordinatedUniversalTime];
        iAUnit *julianDay=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Julian Day", @"Julian Day title") Value:@"#######.#######" IsActivated:YES];
        [allUnits addObject:julianDay];
        iAUnit *location=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Location Coordinate", @"Location Coordinate title") Value:@"Location Service Unavailable" IsActivated:YES];
        [allUnits addObject:location];
        iAUnit *localMeanSolarTime=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Local Mean Solar Time", @"Local Mean Solar Time title") Value:@"Location Service Unavailable" IsActivated:YES];
        [allUnits addObject:localMeanSolarTime];
        iAUnit *localApparentSolarTime=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Local Apparent Solar Time", @"Local Apparent Solar Time title") Value:@"Location Service Unavailable" IsActivated:YES];
        [allUnits addObject:localApparentSolarTime];
        
        iAUnit *localMeanSiderealTime=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Local Mean Sidereal Time", @"Local Mean Sidereal Time title") Value:@"Location Service Unavailable" IsActivated:YES];
        [allUnits addObject:localMeanSiderealTime];
        iAUnit *localApparentSiderealTime=[[iAUnit alloc] initWithTitle:NSLocalizedString(@"Local Apparent Sidereal Time", @"Local Apparent Sidereal Time title") Value:@"Location Service Unavailable" IsActivated:YES];
        [allUnits addObject:localApparentSiderealTime];
    }
    return allUnits;
}

+(void)updateWithLocation:(CLLocation*)currentLocation{
    NSDate *now=[[NSDate alloc] init];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
       
    [dateFormatter setDateFormat:@"EEEE', 'MMM d', 'YYYY GG"];
    ((iAUnit*)[allUnits objectAtIndex:0]).value=[dateFormatter stringFromDate:now];
    
    [dateFormatter setDateFormat:@"HH:mm:ss zzz' (UTC'Z')'"];
    ((iAUnit*)[allUnits objectAtIndex:1]).value=[dateFormatter stringFromDate:now];
    
    NSTimeZone *timeZone=[NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"YYYY-MM-dd' at 'HH:mm:ss"];
    ((iAUnit*)[allUnits objectAtIndex:2]).value=[dateFormatter stringFromDate:now];
    
    NSTimeInterval secondsFromRef=NSTimeIntervalSince1970+[now timeIntervalSinceReferenceDate];
    double julianDay=secondsFromRef/86400+2440587.5;
    /*[dateFormatter setDateFormat:@"y"];
    NSInteger year=[[dateFormatter stringFromDate:now] integerValue];
    [dateFormatter setDateFormat:@"M"];
    int month=[[dateFormatter stringFromDate:now] intValue];
    [dateFormatter setDateFormat:@"d"];
    int day=[[dateFormatter stringFromDate:now] intValue];
    [dateFormatter setDateFormat:@"H"];
    int hour=[[dateFormatter stringFromDate:now] intValue];
    [dateFormatter setDateFormat:@"m"];
    int minute=[[dateFormatter stringFromDate:now] intValue];
    [dateFormatter setDateFormat:@"s"];
    int second=[[dateFormatter stringFromDate:now] intValue];
    NSInteger julianDayYM=year*365.25;
    julianDayYM+=year/400;
    julianDayYM-=year/100;
    julianDayYM+=(month-2)*30.59;
    double julianDay=julianDayYM+day+1721088.5+hour/24.0+minute/1440.0+second/86400.0;*/
    ((iAUnit*)[allUnits objectAtIndex:3]).value=[NSString stringWithFormat:@"%f",julianDay];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    if(currentLocation!=nil){
        //Get current location
        double lat=currentLocation.coordinate.latitude;
        NSString *currentLatitude=@"";
        if(lat<0.0){
            currentLatitude=[NSString stringWithFormat:@"%d°%d\'%0.1f\" S",abs((int)lat),abs((int)((lat-(int)lat)*60)),fabs((lat*60-(int)(lat*60))*60)];
        }
        else if(lat>0.0){
            currentLatitude=[NSString stringWithFormat:@"%d°%d\'%0.1f\" N",abs((int)lat),abs((int)((lat-(int)lat)*60)),fabs((lat*60-(int)(lat*60))*60)];
        }
        else{
            currentLatitude=[NSString stringWithFormat:@"%d°%d\'%0.1f\"",abs((int)lat),abs((int)((lat-(int)lat)*60)),fabs((lat*60-(int)(lat*60))*60)];
        }
        double lon=currentLocation.coordinate.longitude;
        NSString *currentLongitude=@"";
        if(lon<0.0){
            currentLongitude=[NSString stringWithFormat:@"%d°%d\'%0.1f\" W",abs((int)lon),abs((int)((lon-(int)lon)*60)),fabs((lon*60-(int)(lon*60))*60)];
        }
        else if(lat>0.0){
            currentLongitude=[NSString stringWithFormat:@"%d°%d\'%0.1f\" E",abs((int)lon),abs((int)((lon-(int)lon)*60)),fabs((lon*60-(int)(lon*60))*60)];
        }
        else{
            currentLongitude=[NSString stringWithFormat:@"%d°%d\'%0.1f\"",abs((int)lon),abs((int)((lon-(int)lon)*60)),fabs((lon*60-(int)(lon*60))*60)];
        }

         ((iAUnit*)[allUnits objectAtIndex:4]).value=[NSString stringWithFormat:@"%@, %@",currentLatitude,currentLongitude];
        
        NSTimeZone *timeZone=[NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        
        //Calculate Location offset
        NSTimeInterval localSolarTimeMeridian=(currentLocation.coordinate.longitude*24/360)*3600;//in second
        
        NSTimeInterval universalTime=0.0;
        //get hours
        [dateFormatter setDateFormat:@"HH"];
        int hour=[[dateFormatter stringFromDate:now] intValue];
        universalTime+=3600.0*hour;
        //get minutes
        [dateFormatter setDateFormat:@"mm"];
        int minute=[[dateFormatter stringFromDate:now] intValue];
        universalTime+=60*minute;
        //get seconds
        [dateFormatter setDateFormat:@"ss"];
        int second=[[dateFormatter stringFromDate:now] intValue];
        universalTime+=second;
        
        //Calculate local mean solar time
        NSTimeInterval localMeanSolarTime=universalTime+localSolarTimeMeridian;

        //add 1 day if there is a negative time
        if(localMeanSolarTime<0.0){
            localMeanSolarTime+=86400;
        }else if(localMeanSolarTime>86400){
            localMeanSolarTime-=86400;
        }
        
        ((iAUnit*)[allUnits objectAtIndex:5]).value=[NSString stringWithFormat:@"%d:%02d:%02d",(int)(localMeanSolarTime/3600),((int)localMeanSolarTime%3600)/60,((int)localMeanSolarTime%3600)%60];
        
        //Calculation of Local Apparent Solar Time
        //calculate equation of time
        [dateFormatter setDateFormat:@"D"];
        double dtor=2*3.141592653589793238462643383279502884194/360;
        double B=dtor*360/365*([[dateFormatter stringFromDate:now] intValue]-81);
        double equationOfTime=9.87*sin(2.0*B)-7.53*cos(B*1.0)-1.5*sin(B);//in minutes
        //convert to seconds
        equationOfTime=equationOfTime*60;
        
        //Calculate local apparent solar time
        NSTimeInterval localApparentSolarTime=universalTime+equationOfTime+localSolarTimeMeridian;
        
        //add 1 day if there is a negative time
        if(localApparentSolarTime<0.0){
            localApparentSolarTime+=86400;
        }else if(localApparentSolarTime>86400){
            localApparentSolarTime-=86400;
        }
        
        ((iAUnit*)[allUnits objectAtIndex:6]).value=[NSString stringWithFormat:@"%d:%02d:%02d",(int)(localApparentSolarTime/3600),((int)localApparentSolarTime%3600)/60,((int)localApparentSolarTime%3600)%60];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        
        //Calculation of Local Mean Sidereal Time
        NSTimeInterval greenwichMeanSiderealTime=(18.697374558 + 24.06570982441908*(julianDay-2451545.0));
        //greenwichMeanSiderealTime=(greenwichMeanSiderealTime-(int)greenwichMeanSiderealTime)*24;//in hours
        NSTimeInterval localMeanSiderealTime=greenwichMeanSiderealTime+currentLocation.coordinate.longitude/15;
        //NSLog([NSString stringWithFormat:@"%f",localMeanSiderealTime]);
        localMeanSiderealTime=(localMeanSiderealTime/24-(int)(localMeanSiderealTime/24))*24;
        ((iAUnit*)[allUnits objectAtIndex:7]).value=[NSString stringWithFormat:@"%d:%02d:%02d",(int)(localMeanSiderealTime),(int)((localMeanSiderealTime-(int)localMeanSiderealTime)*60),(int)(localMeanSiderealTime*3600)%60];
        
        //Calculation of Local Apparent Sidereal Time
        double d=julianDay-2451545.0;
        //Calculation of equation of equinoxes
        
        double eqeq=(-0.000319*sin((125.04-0.052954*d)*dtor)-0.000024*sin(dtor*(2*(280.47+0.98565*d))))*cos((23.4393-0.0000004*d)*dtor);
        //NSLog([NSString stringWithFormat:@"%f",eqeq]);
        NSTimeInterval greenwichApparentSideralTime=greenwichMeanSiderealTime+eqeq;
        NSTimeInterval localApparentSiderealTime=greenwichApparentSideralTime+currentLocation.coordinate.longitude/15;
        localApparentSiderealTime=(localApparentSiderealTime/24-(int)(localApparentSiderealTime/24))*24;
        ((iAUnit*)[allUnits objectAtIndex:8]).value=[NSString stringWithFormat:@"%d:%02d:%02d",(int)(localApparentSiderealTime),(int)((localApparentSiderealTime-(int)localApparentSiderealTime)*60),(int)(localApparentSiderealTime*3600)%60];
    }
}

@end
