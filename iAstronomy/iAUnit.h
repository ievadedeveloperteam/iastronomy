//
//  iAUnit.h
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iAUnit : NSObject{
    bool isActivated;
    NSString* title;
    NSString* value;
}

@property(readwrite) bool isActivated;
@property(nonatomic,retain) NSString* title;
@property(nonatomic, retain) NSString* value;

-(id)initWithTitle:(NSString*)t Value:(NSString*)v IsActivated:(bool)ia;

@end
