//
//  iATime.h
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface iATime : NSObject{
    
}



+(NSArray *)allUnits;
+(void)updateWithLocation:(CLLocation*)currentLocation;


@end
