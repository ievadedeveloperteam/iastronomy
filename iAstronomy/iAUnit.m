//
//  iAUnit.m
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import "iAUnit.h"

@implementation iAUnit

@synthesize isActivated;
@synthesize title;
@synthesize value;

-(id)initWithTitle:(NSString*)t Value:(NSString*)v IsActivated:(bool)ia{
    if(self=[super init]){
        self.isActivated=ia;
        self.title=t;
        self.value=v;
    }
    return self;
}

@end
