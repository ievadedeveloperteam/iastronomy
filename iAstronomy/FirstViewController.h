//
//  FirstViewController.h
//  iAstronomy
//
//  Created by Hao He on 13-2-8.
//  Copyright (c) 2013年 Hao He. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FirstViewController : UITableViewController<CLLocationManagerDelegate>{
    NSTimer *updateTimer;
    CLLocationManager *locationManager;
    CLLocation * currentLocation;
}


@property(nonatomic,retain) NSTimer* updateTimer;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain)CLLocation * currentLocation;

-(void)update;

@end
